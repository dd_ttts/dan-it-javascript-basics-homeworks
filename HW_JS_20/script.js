block_1: {
    const arr = [];
    const first = {
        firstName: "Ivan",
        lastName: "Kalosha",
        age: 31,
        proger: true,
        hobbies: ["videogames", "bike", "enduro", "physic", "ski"],
    };
    const second = {
        firstName: "Petro",
        lastName: "Galushka",
        age: 32,
        developer: true,
        hobbies: ["videogames", "motorcycles", "enduro", "cigars"],
    };
    const third = {
        firstName: "Vasyl",
        lastName: "Cybulja",
        age: 60,
        pensioner: true,
        hobbies: ["VideoGames"],
        friends: [first, second],
    };
    arr.push(first);
    arr.push(second);
    arr.push(third);
    let search;
    // search = "videogames";
    // search = "VideoGames vasyl";
    // search = "videogames vasyl";
    // search = "videogames Boris";
    // search = "Mykola vasyl";
    search = "Bike anton";
    const sortArr = filterCollection(arr, search, true, "friends.firstName", "friends.hobbies");
    console.log("Search: " + search);
    console.log(sortArr);
}

function filterCollection(arr, str, flag) {
    const result = [];
    const searchWords = getSearchWords(str);
    const keys = [];
    for (let i = 3; i < arguments.length; i++) {
        keys.push(arguments[i]);
    }
    for (let item of arr) {
        if (filter(item, searchWords, keys, flag)) {
            result.push(item);
        }
    }
    if (result.length == 0) {
        return undefined;
    }
    else {
        return result;
    }
}

function getSearchWords(str) {
    const arr = str.split(" ");
    for (let i = 0; i < arr.length; i++) {
        arr[i] = arr[i].toLowerCase();
    }
    return arr;
}

function filter(obj, words, keys, flag) {
    if (Array.isArray(obj)) {
        for (let item of obj) {
            if (filter(item, words, keys, flag)) {
                return true;
            }
        }
        return false;
    }
    else {
        return finder(obj, words, keys, flag);
    }
}

function finder(element, words, keys, flag) {
    let counter = 1;
    if (flag) {
        counter = words.length;
    }
    for (let word of words) {
        if (coincidence(element, word, keys)) {
            counter--;
        }
    }
    if (counter > 0) {
        return false;
    }
    return true;
}

function coincidence(obj, word, keys) {
    if (keys.length > 0) {
        for (let key of keys) {
            if (hardCoincidence(obj, word, key.split("."))) {
                return true;
            }
        }
    }
    else {
        for (let item in obj) {
            if (easyCoincidence(obj[item], word)) {
                return true;
            }
        }
    }
    return false;
}

function hardCoincidence(some, word, deep) {
    if (Array.isArray(some)) {
        for (let item of some) {
            if (hardCoincidence(item, word, deep)) {
                return true;
            }
        }
        return false;
    }
    else {
        if (deep.length == 1) {
            return easyCoincidence(some[deep[0]], word);
        }
        else {
            if (some.hasOwnProperty(deep[0])) {
                return hardCoincidence(some[deep[0]], word, deep.slice(1));
            }
            else {
                return false;
            }
        }
    }
}

function easyCoincidence(some, word) {
    if (typeof(some) == "object") {
        if (Array.isArray(some)) {
            for (let item of some) {
                if (easyCoincidence(item, word)) {
                    return true;
                }
            }
            return false;
        }
        else {
            for (let key in some) {
                if (easyCoincidence(some[key], word)) {
                    return true;
                }
            }
            return false;
        }
    }
    else {
        if (typeof(some) == "string") {
            if (some.toLowerCase() == word) {
                return true;
            }
        }
        return false;
    }
}
