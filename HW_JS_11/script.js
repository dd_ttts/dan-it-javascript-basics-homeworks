let eyeIcons = document.querySelectorAll("i");
for (eye of eyeIcons) {
    eye.onclick = eyeClick;
}
let button = document.querySelector("button");
button.onclick = checkPass;


function eyeClick(e)  {
    if (e.target.classList.contains("fa-eye")) {
        e.target.className = "fas fa-eye-slash icon-password";
        showPassword(e.target.previousElementSibling, "text");
    }
    else {
        e.target.className = "fas fa-eye icon-password";
        showPassword(e.target.previousElementSibling, "password");
    }
}

function showPassword(elem, str) {
    elem.type = str;
}

function checkPass() {
    let inputStrings = document.querySelectorAll("input");
    if (inputStrings[0].value == "" || inputStrings[1].value == "") {
        alert("Заповніть обидва поля!");
    }
    else if (inputStrings[0].value == inputStrings[1].value) {
        let alertStr = document.querySelector("p");
        alertStr.className = "white-text";
        alert("You are  welcome!");
    }
    else {
        let alertStr = document.querySelector("p");
        if (alertStr) {
            alertStr.className = "red-text";
        }
    }
}