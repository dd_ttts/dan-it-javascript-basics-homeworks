/*

Теорія

1.  В JavaScript існують такі типи даних:
    - строка
    - число
    - об'єкт
    - null
    - undefined
    - NaN
    - булеві (true/false)

    Також є більш рідко вживані:
    - символ
    - великі числа

2.  Оператор порівняння "==" виконує приведення типів даних при порівнянні, тому наприклад 0 (число) буде дорівнювати "false" (логічне)
    Оператор строгого порівняння "===" не виконує приведення типів, тому "null" та "undefined" не будуть рівні, як при використанні "=="

3.  Оператор - це функція мови програмування, яка виконує деякі дії з тими об'єктами, до яких ми її застосовуємо, і повертає нам результат цих дій

*/

{
    let inputString = prompt("Hi! Input your name and age here!");
    let user = inputString.split(" ");
    while (user.length < 2 || Number(user[0]) || !Number(user[1])) {
        inputString = prompt("Hi! Input your name and age here!", inputString);
        user = inputString.split(" ");
    }
    if (+user[1] < 18) {
        alert("You are not allowed to visit this website!");
    }
    else if (+user[1] < 23) {
        confirm("Are you sure you want to continue?") ? alert("Welcome, " + user[0]) : alert("You are not allowed to visit this website!");
    }
    else {
        alert("Welcome, " + user[0]);
    }
}
