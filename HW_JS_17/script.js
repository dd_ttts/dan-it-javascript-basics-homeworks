block_1: {
    const student = {
        firstName: "",
        lastName: "",
    }
    student.firstName = input_name("Input student's first name");
    if (student.firstName == undefined) {
        break block_1;
    }
    student.lastName = input_name("Input student's last name");
    if (student.lastName == undefined) {
        break block_1;
    }
    student.tabel = create_tabel();
    if (!student.tabel) {
        alert("Student's table is empty!");
        break block_1;
    }
    student.getBadRatings = function() {
        let badRating = 0;
        for (let discipline in this.tabel) {
            if (this.tabel[discipline] < 4) {
                badRating++;
            }
        }
        return badRating;
    };
    student.getGradePointAverage = function() {
        let counter = 0;
        let sumPoints = 0;
        for (let discipline in this.tabel) {
            counter++;
            sumPoints += this.tabel[discipline];
        }
        return (sumPoints / counter).toFixed(1);
    };
    if (student.getBadRatings() === 0) {
        confirm("Студента переведено на наступній курс");
    }
    if (student.getGradePointAverage() > 7) {
        confirm("Студенту призначено стипендію");
    }
}

function input_name (str) {
    let name;
    while (!name || !/[a-z]+/i.test(name)) {
        name = prompt(str, name);
        if (name === null) {
            return undefined;
        }
    }
    return name;
}

function create_tabel() {
    let tabel = {};
    while (true) {
        let discipline;
        let point;
        while (!discipline || !/[a-z]+/i.test(discipline)) {
            discipline = prompt("Input discipline", discipline);
            if (discipline === null) {
                break;
            }
        }
        if (discipline === null) {
            break;
        }
        while (!point || /\D/.test(point)) {
            point = prompt("Input points for discipline: " + discipline);
        }
        tabel[discipline] = Number(point);
    }
    if (Object.keys(tabel).length == 0) {
        return undefined;
    }
    return tabel;
}