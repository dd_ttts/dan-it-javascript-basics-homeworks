/*

Теорія

1.  Створити новий HTML-тег на сторінці можна за допомогою наступного алгоритму:
    1) Створити необхідний тег за допомогою методу "document.createElement(tag)";
    2) Додати до нього необхидний вміст за допомогою "innerHTML" або "innerText";
    3) Вставити за допомогою методів ".append()", ".prepend()", ".before()" та ".after()".

    Або за допомогою методу ".insertAdjacentHTML()".

2.  Функція element.insertAdjacentHTML(firstParameter, secondParameter) приймає першим параметром "firstParameter" один з чотирьох варіантів,
    кожен з яких вказує функції, куди саме необхідно вставити контент "secondParameter" відносно елемента, на який функція була викликана:
    1) "beforebegin"    - вставляє "secondParameter" перед елементом "element" в батьківський тег,
                          таким чином "secondParameter" стає сусідом зліва для "element": <secondParameter>...</secondParameter><element>...</element>
    2) "afterend"       - вставляє "secondParameter" після елементу "element" в батьківський тег,
                          таким чином "secondParameter" стає сусідом зправа для "element": <element>...</element><secondParameter>...</secondParameter>
    3) "afterbegin"     - вставляє "secondParameter" в середину "element" таким чином, що "secondParameter" стає першим нащадком "element":
                          <element><secondParameter>...</secondParameter>...</element>
    4) "beforeend"      - вставляє "secondParameter" в середину "element" таким чином, що "secondParameter" стає останнім нащадком "element":
                          <element>...<secondParameter>...</secondParameter></element>

3.  Видалити елемент зі сторінки можна за допомогою методу ".remove()".
    
*/

block_1: {
    const arr = ["hello", "second", "3rd", ["4.1 - aaaa", "4.2 - bbbb", "4.3 - cccc", ["4.4 - A", "4.4 - B", "4.4 - C"]], 666, "LAST"];
    let parent = document.querySelector('div');
    createNewContent(arr, parent);
    parent.insertAdjacentHTML("beforebegin", "<h2>3</h2>");
    let timer = document.querySelector('h2');
    let current = 2;
    let time = function() {
        timer.innerText = current;
        current--;
    }
    let countdown = setInterval(time, 1000);
    setTimeout(() => clearInterval(countdown), 3000);
    setTimeout((elem) => elem.remove(), 3000, document.querySelector("ul"));
}

function createNewContent(arr, parent = document.body) {
    let list = document.createElement('ul');
    parent.append(list);
    for (let item of arr) {
        let elem = document.createElement('li');
        list.append(elem);
        if (Array.isArray(item)) {
            createNewContent(item, elem);
        }
        else {
            elem.innerText = item;
        }
    }
}