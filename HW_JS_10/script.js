
//  Tabs

let tabs = document.querySelector('.tabs').children;
let contents = document.querySelector('.tabs-content').children;
for (let i = 0; i < tabs.length; i++) {
    tabs[i].setAttribute("data-index", i + 1);
    contents[i].setAttribute("data-index", i + 1);
}
for (tab of tabs) {
    tab.onclick = handleClick;
}

function handleClick(eventObject) {
    for (let item of tabs) {
        item.className = "tabs-title";
    }
    eventObject.target.classList.add("active");
    contentSwitch(eventObject.target.dataset.index);
}

function contentSwitch(tabIndex) {
    for (let item of contents) {
        item.className = "non-content";
        if (item.dataset.index == tabIndex) {
            item.className = "";
        }
    }
}
