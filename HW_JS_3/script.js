/*

Теорія

1.  Цикли потрібні для повторного виконання однієї і тієї ж дії задану кількість разів або поки не буде виконана певна умова

2.  Цикл for (...; ...; ...;) {...} зручно використовувати для роботи з числами (наприклад індексами або лічильниками)
    Цикл while (...) {...} стає у нагоді, коли умови для роботи циклу не пов'язані з числами і можуть бути приведені до
    булевих значень true/false

3.  Явне приведення типів, це процес перетворення одного типу даних на інший тип, використовуючі спеціальні функції, наприклад
        {
            let str = "2022"; --> typeof str == "string"
            str = Number(str); --> typeof str == "number"
        }

    Неявне приведення типів - це перетворення без використання спеціальних функцій, наприклад 
        {
            let str = "199"; - строка "199"
            let num = +str; - число 199
            
            чи навпаки

            let num = 1991;
            let str = "" + num; --> str == "1991"
        }
        або
        {
            if (1) {...} - значення в двужках не є булевим, але буде розпізнано як "true"
        }

*/

block_1: {
    let inputNum = prompt("Введіть будь-яке ціле число");
    if (inputNum !== null) {
        while (!Number(inputNum) || Number(inputNum) % 1 != 0) {
            inputNum = prompt("Введіть будь-яке ціле число", inputNum);
            if (inputNum === null) {
                break block_1;
            }
        }
        inputNum = Number(inputNum);
        const arrFive = [];
        if (inputNum > 0) {
            for (let i = 5; i <= inputNum; i += 5) {
             arrFive.push(i);
            }
        }
        else {
            for (let i = -5; i >= inputNum; i -= 5) {
                arrFive.push(i);
            }
        }
        if (arrFive.length != 0) {
            let result = "";
            for (let i = 0; i < arrFive.length; i++) {
                result += arrFive[i] + "; ";
            }
            console.log(result);
        }
        else {
            alert("Sorry, no numbers");
        }
    }
}

block_2: {
    let inputNums = prompt("Введіть два додатних цілих числа");
    if (inputNums === null) {
        break block_2;
    }
    while (inputNums.match(/\S+/g).length != 2 || /[^0-9 ]/.test(inputNums)) {
        inputNums = prompt("Введіть два цілих числа", inputNums);
        if (inputNums === null) {
            break block_2;
        }
    }
    inputNums = inputNums.match(/\S+/g);
    let min = Number(inputNums[0]);
    let max = Number(inputNums[1]);
    if (min > max) {
        let temp = min;
        min = max;
        max = temp;
    }
    if (max < 2) {
        console.log("Max number is too small...");
        break block_2;
    }
    if (max == 2) {
        console.log("Only 2");
        break block_2;
    }
    const arrSimple = [2];
    const arrResult = [];
    for (let i = 3; i < min; i += 2) {
        if (isSimple(i, arrSimple)) {
            arrSimple.push(i);
        }
    }
    if (min <= 2) {
        min = 2;
        arrResult.push(2);
    }
    for (let j = min; j <= max; j++) {
        if (isSimple(j, arrSimple)) {
            arrSimple.push(j);
            arrResult.push(j);
        }
    }
    let result = "";
    for (let i = 0; i < arrResult.length; i++) {
        result += arrResult[i] + "; ";
    }
    console.log(result);
}

function isSimple(num, arrNums) {
    for (let i = 0; i < arrNums.length; i++) {
        if (num % arrNums[i] == 0) {
            return false;
        }
    }
    return true;
}