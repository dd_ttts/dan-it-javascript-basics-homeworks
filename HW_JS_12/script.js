/*

Теорія

1.  В наш час є не один спосіб введення інформації в поле "input" (наприклад голосове введення або вставка скопійованого тексту за допомогою миші),
    тому для роботи з цим полем не слід обмежуватись одними клавіатурними подіями.

*/

document.addEventListener('keypress', keyColor);

function keyColor(e) {
    let buttons = document.querySelectorAll("button");
    let keyNumber = checkKey(e.code);
    if (keyNumber >= 0) {
        for (button of buttons) {
            button.className = "btn";
        }
        buttons[keyNumber].classList.add("blue");
    }
}

function checkKey(key) {
    switch (key) {
        case "Enter": return 0;
        case "KeyS": return 1;
        case "KeyE": return 2;
        case "KeyO": return 3;
        case "KeyN": return 4;
        case "KeyL": return 5;
        case "KeyZ": return 6;
    }
    return -1;
}