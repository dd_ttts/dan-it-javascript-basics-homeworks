/*

Теорія

1.  Екранування - це засіб "вимикання" спецсимволів, коли вони потрібні в якості просто свого символу в тексті,
    або навпаки, коли ми потребуємо викликати саме спецсимвол. Викликається символом зворотнього слеша "\"
    Наприклад:

    const str = "First string!\nSecond String!"; ---> тут "\n" - спецсимвол, що при виведенні на екран розділить строку на 2 абзаци

    const regex = /[0-9]\./i; ---> тут "\" екранує точку ".", щоб вона сприймалася кодом саме я к символ точки

2.  Засоби оголошення функцій:
    1) function do_something (arguments) {...};                 Функція-декларація
    2) const do_somthing_else = function (arguments) {...};     Функція-експресія
    3) const little_func = (arg1, arg2) => arg1 + arg2;         Функція-стрілка

3.  hosting - це фаза створення (компіляції) оголошених змінних або функцій, що використовуються кодом, до початку безпосередьньої роботи з ними
    Це дозволяє використовувати такі змінні та функції в коді до того, як вони були оголошені. Код ніжче ярко демонструє цей принцип.
    Без нього було б потрібно оголошувати функції перед початком їх виклику.

*/

{
    const user1 = createNewUser();
    if (user1) {
        console.log(user1);
        console.log(user1.getAge());
        console.log(user1.getPassword());
    }
}

function createNewUser() {
    let inputStr;
    while (!inputStr || /^[a-z]+ [a-z]+$/i.test(inputStr) != true) {
        inputStr = prompt("Введіть ім'я та прізвище користувача");
        if (inputStr === null) {
            break;
        }
    }
    if (inputStr) {
        inputStr = inputStr.split(" ");
        const newUser = {
            firstName: inputStr[0],
            lastName: inputStr[1],
            getLogin() {
                const shortLogin = this.firstName[0] + this.lastName;
                return shortLogin.toLowerCase();
            },
            setFirstName: function(newFirstName) {
                if (/\w+/.test(newFirstName)) {
                    Object.defineProperty(newUser, "firstName", {
                        writable: true,
                    });
                    this.firstName = newFirstName;
                    Object.defineProperty(newUser, "firstName", {
                        writable: false,
                    });
                }
            },
            setLastName: function(newLastName) {
                if (/\w+/.test(newLastName)) {
                    Object.defineProperty(newUser, "lastName", {
                        writable: true,
                    });
                    this.lastName = newLastName;
                    Object.defineProperty(newUser, "lastName", {
                        writable: false,
                    });
                }
            },
        };
        Object.defineProperty(newUser, "firstName", {
            writable: false,
        });
        Object.defineProperty(newUser, "lastName", {
            writable: false,
        });
        //
        //      Homework 6
        //      <---start--->
        //
        let inputBirthady;
        while (!inputBirthady || !check_birthday(inputBirthady)) {
            inputBirthady = prompt('Введіть дату народження в форматі: дд.мм.рррр', inputBirthady);
            if (inputBirthady === null) {
                inputBirthady = undefined;
                break;
            }
        }
        inputBirthady = inputBirthady.split(".");
        newUser.birthday = new Date (inputBirthady[2] + "-" + inputBirthady[1] + "-" + inputBirthady[0]);
        newUser.getAge = function() {
            const now = new Date();
            return now.getFullYear() - this.birthday.getFullYear();
        };
        newUser.getPassword = function() {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.getAge();
        };
        //
        //      Homework 6
        //      <---end--->
        //
        return newUser;
    }
}

        //
        //      New function to homework 6
        //

function check_birthday(str) {
    if (!str) {
        return false;
    }
    if (!/[^0-9|\.]/.test(str) && str.length == 10) {
        let separate = str.split(".");
        let num = Number(separate[0]);
        if (num > 0 && num < 32) {
            num = Number(separate[1]);
            if (num > 0 && num < 13) {
                return true;
            }
        }
    }
    return false;
}
