/*

Теорія

1.  Метод "forEach()"" виконує передану в нього в якості аргументу функцію з кожним елементом масиву

2.  Найкоротший засоб - це обнулення параметру "lenght": arr.lenght = 0;
    Також можна використати метод "splice": arr.splice(0);

3.  Для перевірки, чи є змінна масивом, необхідно використовувати метод "isArray": Array.isArray(variable) => true / false;

*/

block_1: {
    const exampleArr = ["start", 1, 2, 3, "four", "five", [7, 8, 9], null, 89n, {id: "example"}, 10, 11, NaN, "twelve", false, 666, true, undefined, "end"];
    let whatType = ["number", "bigint", "string", "boolean", "undefined", "null", "NaN", "object"];
    console.log("Example Array:\n");
    console.log(exampleArr);
    console.log("\n\n");
    for (let typeStr of whatType) {
        let filtredArr = filterBy(exampleArr, typeStr);
        console.log(filtredArr);
        if (checkResult(filtredArr, typeStr)) {
            console.log(typeStr + " in new Array: no");
        }
        else {
            console.log("Function work wrong for type: " + typeStr);
        }
    }
}

function filterBy(arr, str) {
    const result = [];
    for (let item of arr) {
        if (!checkType(item, str)) {
            if (typeof(item) == "object" && item !== null) {
                result.push(clone_obj(item));
            }
            else {
                result.push(item);
            }
        }
    }
    if (result.length > 0) {
        return result;
    }
    else {
        return undefined;
    }
}

// number, bigint, string, boolean, undefined, object   -> typeof work right
// NaN                                                  -> typeof return "number"
// null                                                 -> typeof return "object"

function checkType (item, str) {
    if (item === null) {
        if (str == "null") {
            return true;
        }
        return false;
    }
    else if (typeof(item) == "number" && isNaN(item) && str == "NaN") {
        return true;
    }
    else if (typeof(item) == str) {
        return true;
    }

    return false;
}

function checkResult (arr, str) {
    for (let item of arr) {
        if (checkType(item, str)) {
            return  false;
        }
    }
    return true;
}

// * * * * *    From homework 18    * * * * * //

function clone_obj (obj) {
    if (Array.isArray(obj)) {
        const copyOfObj = [];
        for (let i = 0; i < obj.length; i++) {
            if (typeof(obj[i]) == "object") {
                copyOfObj.push(clone_obj(obj[i]));
            }
            else {
                copyOfObj.push(obj[i]);
            }
        }
        return copyOfObj;
    }
    else if (typeof(obj) == "object" && obj !== null) {
        const copyOfObj = {};
        for (const key in obj) {
            if (typeof(obj[key]) == "object" && obj[key] !== null) {
                copyOfObj[key] = clone_obj(obj[key]);
            }
            else {
                copyOfObj[key] = obj[key];
            }
        }
        return copyOfObj;
    }
    else {
        return obj;
    }
}

// * * * * *    From homework 18    * * * * * //