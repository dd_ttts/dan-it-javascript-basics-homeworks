/*

Теорія

1.  DOM (Document Object Model) - це модель документа (сторінки), яка створюється браузером при читанні html-файлу.
    В цій моделі всі елементи html-файлу (теги, коментарі та інше) перетвоюються на об'єкти з відповідними атрибутами
    та властивостями, якими можна маніпулювати за допомогою JavaScript.

2.  innerHTML - властивість дозволяє отримати або встановити текстовий вміст з html-розміткою в середині html-елемента;
    innerText - властивість дозволяє отримати або встановити текстовий вміст html-елемента та його нащадків БЕЗ власне самої html-розмітки.

3.  До елементів сторінки можна звернутись за допомогою метода .getElementById(), але для цього в елемента має бути встановлен атрибут "id",
    до того ж він має бути унікальним.
    Метод .querySelector() є більш гнучким, не потребує обов'язкового атрубуту "id", а також може знаходити не один елемент, а одразу кілька
    за допомогою .querySelectorAll(), тому загалом він є трохи кращім.

*/

block_1: {
    //  1:
    let paragraphs = document.querySelectorAll('p');
    paragraphs.forEach(some => some.style.backgroundColor = '#ff0000');
    //  2:
    let consoleView = document.getElementById('optionsList');
    console.log(consoleView);
    console.log(consoleView.parentElement);
    for (let kid of consoleView.childNodes) {
        console.log(kid.nodeName + ' + ' + kid.nodeType);
    }
    //  3:
    let elemetTestParagraph = document.getElementById('testParagraph');
    elemetTestParagraph.innerHTML = "This is a paragraph";
    //  4:
    let mainHeaderChildren = document.querySelector('.main-header').children;
    for (let kid of mainHeaderChildren) {
        console.log(kid);
        kid.className = "nav-item";
    }
    //  5:
    let titles = document.querySelectorAll('.section-title');
    titles.forEach(elem => elem.classList.remove('section-title'));
}
