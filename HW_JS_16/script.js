block_1: {
    let num0 = input_number("Введіть перше число послідовності, з якого почнеться відлік");
    if (num0 === undefined) {
        break block_1;
    }
    let num1 = input_number("Введіть наступне чісло послідовності");
    if (num1 === undefined) {
        break block_1;
    }
    let n = input_number("Введіть порядковий номер числа послідовності, яке треба вирахувати");
    if (n === undefined) {
        break block_1;
    }
    console.log("Ваше число в послідовності: " + fibonacci(num0, num1, n));
}

function input_number (str) {
    let num;
    while (!num || !/-{0,1}\d+/.test(num)) {
        num = prompt(str, num);
        if (num === null) {
            return undefined;
        }
    }
    return Number(num);
}

function fibonacci (nullNum, firstNum, n) {
    if (n <= 0) {
        if (n == 0) {
            return nullNum;
        }
        else {
            return fibonacci (firstNum - nullNum, nullNum, n + 1);
        }
    }
    else {
        if (n == 1) {
            return firstNum;
        }
        else {
            return fibonacci (firstNum, nullNum + firstNum, n - 1);
        }
    }
}