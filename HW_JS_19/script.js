{
    const deadline = new Date("2023-01-15");
    const workers = [2, 3, 5];
    const work = [10, 20, 40, 100, 30, 10];
    checkDeadline(workers, work, deadline);
}

// Функція не перевіряє коректність вхідних даних, тому для правильного спрацювання мають бути дотримані умови:
// "workSpeed" - вхідний масив швидкості роботи членів команди не може бути порожнім та не може містити від'ємні значення;
// "backlog" - вхідний масив об'ємів роботи не може містити від'ємні значення;
// "deadline" - дата дедлайну не може бути в минулому;

function checkDeadline (workSpeed, backlog, deadline) {
    const haveDays = workDays(deadline);
    const needDays = workScope(workSpeed, backlog);
    const result = haveDays - needDays;
    if (result >= 0) {
        confirm(`Усі завдання будуть успішно виконані за ${result} робочих днів до настання дедлайну!`);
    }
    else {
        confirm(`Команді розробників доведеться витратити додатково ${Math.round(result * -8)} годин після дедлайну, щоб виконати всі завдання в беклозі`);
    }
}

function workDays(finish) {
    let current = new Date();
    let days = 0;
    while (finish - current > 0) {
        if (current.getDay() > 0 && current.getDay() < 6) {
            days++;
        }
        current.setDate(current.getDate() + 1);
    }
    return days;
}

function workScope(workers, work) {
    const oneDayWork = workers.reduce((accum, current) => accum + current, 0);
    const sumWork = work.reduce((accum, current) => accum + current, 0);
    return (sumWork / oneDayWork).toFixed(1);
}