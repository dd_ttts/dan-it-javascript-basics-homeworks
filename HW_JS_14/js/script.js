let button = document.getElementById("changeButton");
button.onclick = changeColor;
checkColor(localStorage.getItem("theme"));


function changeColor() {
    document.body.classList.toggle("grey-city");
    button.classList.toggle("darkgreen-button");
    let contacts = document.querySelectorAll(".contact");
    let logo = document.querySelector(".logo-box");
    let tourBoxes = document.querySelectorAll(".tour-box");
    logo.classList.toggle("darkgreen");
    for (let contact of contacts) {
        contact.classList.toggle("darkgreen");
    }
    for (let box of tourBoxes) {
        box.classList.toggle("darkgreen");
    }
    if (document.body.classList.contains("grey-city")) {
        localStorage.setItem("theme", "darkgreen");
    }
    else {
        localStorage.setItem("theme", "orange");
    }
}

function checkColor(str) {
    if (str == "darkgreen" && !document.body.classList.contains("grey-city")) {
        changeColor();
    }
    if (str == "orange" && document.body.classList.contains("grey-city")) {
        changeColor();
    }
}