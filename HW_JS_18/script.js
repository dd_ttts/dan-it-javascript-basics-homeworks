block_1: {
    const example = {
        firstName: "Viktor",
        lastName: "Pavlik",
        first: {
            ololo: "Nit",
            alala: "Dat",
            really: {
                aaaa: 4,
                bbbb: 5,
            },
        },
        second: {
            tratata: 666,
            atata: 999,
        },
        third: [3, 33, 333, {}, [2, 22, 222]],
    };
    const copy = clone_obj(example);
    console.log(example);
    console.log(copy);
}

function clone_obj (obj) {
    if (Array.isArray(obj)) {
        const copyOfObj = [];
        for (let i = 0; i < obj.length; i++) {
            if (typeof(obj[i]) == "object") {
                copyOfObj.push(clone_obj(obj[i]));
            }
            else {
                copyOfObj.push(obj[i]);
            }
        }
        return copyOfObj;
    }
    else if (typeof(obj) == "object" && obj !== null) {
        const copyOfObj = {};
        for (const key in obj) {
            if (typeof(obj[key]) == "object" && obj[key] !== null) {
                copyOfObj[key] = clone_obj(obj[key]);
            }
            else {
                copyOfObj[key] = obj[key];
            }
        }
        return copyOfObj;
    }
    else {
        return obj;
    }
}