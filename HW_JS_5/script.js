/*

Теорія

1.  Метод об'єкту - це функція, яка прописана в самому об'єкті як одна з його властивостей і зазвичай працює з властивостями
    об'єкту, не використовуючи зовнішніх змінних або даних (окрім переданих в саму функцію під час її виклику).
    Для виклику методу об'єкта використовується такий самий синтаксис, що й для доступу до внутрішніх змінних цього об'єкта:
    Object.method();

2.  Властивість об'єкта може мати будь-який тип даних: від найпростіших чисел чи строк до інших об'єктів або функцій.

3.  Об'єкт є посілальним типом данних, що означає наступне: при створенні об'єкту, змінна, якій він присвоюється,
    насправі отримує не сам об'єкт, як це було б з простим типом даних накшталт строки, а адресу на ділянку пам'яті,
    де насправді створюється та зберігається об'єкт. Тому при маніпулюванні зі змінною (напріклад її копіювання в іншу змінну),
    ми манипулюємо з адресою об'єкта, а ні з ним самим. Щоб впливати на сам об'єкт, необхідно звертатися до його властивостей
    напряму через синтаксис Object.property або Object["property"].

*/

function createNewUser() {
    let inputStr;
    while (!inputStr || /^[a-z]+ [a-z]+$/i.test(inputStr) != true) {
        inputStr = prompt("Введіть ім'я та прізвище користувача");
        if (inputStr === null) {
            break;
        }
    }
    if (inputStr) {
        inputStr = inputStr.split(" ");
        const newUser = {
            firstName: inputStr[0],
            lastName: inputStr[1],
            getLogin() {
                const shortLogin = this.firstName[0] + this.lastName;
                return shortLogin.toLowerCase();
            },
            setFirstName: function(newFirstName) {
                if (/\w+/.test(newFirstName)) {
                    Object.defineProperty(newUser, "firstName", {
                        writable: true,
                    });
                    this.firstName = newFirstName;
                    Object.defineProperty(newUser, "firstName", {
                        writable: false,
                    });
                }
            },
            setLastName: function(newLastName) {
                if (/\w+/.test(newLastName)) {
                    Object.defineProperty(newUser, "lastName", {
                        writable: true,
                    });
                    this.lastName = newLastName;
                    Object.defineProperty(newUser, "lastName", {
                        writable: false,
                    });
                }
            },
        };
        Object.defineProperty(newUser, "firstName", {
            writable: false,
        });
        Object.defineProperty(newUser, "lastName", {
            writable: false,
        });
        return newUser;
    }
}

{
    const user1 = createNewUser();
    if (user1) {
        console.log("Викликаємо геттер об'єкта: " + user1.getLogin());
        user1.firstName = "Michael";
        user1.lastName = "Jordan";
        console.log("Спробуємо змінити ім'я на Michael Jordan та викликати геттер: " + user1.getLogin());
        user1.setFirstName("Jack");
        user1.setLastName("Daniels");
        console.log("Спробуємо змінити ім'я за допомогою сеттерів на Jack Daniels та викликати геттер: " + user1.getLogin());
    }
}
